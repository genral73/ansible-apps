#!/usr/bin/env bash
docker rm --force mp-pgbouncer
docker run --platform linux/amd64 \
  --name mp-pgbouncer  \
  -e "POSTGRESQL_HOST=mrkt-local-db" \
  -e "POSTGRESQL_PORT=5432" \
  -e "PGBOUNCER_PORT=6432" \
  -e "POSTGRESQL_PASSWORD=Aa123456!" \
  -e "PGBOUNCER_DATABASE=*" \
  -e "PGBOUNCER_POOL_MODE=statement" \
  -e "PGBOUNCER_IGNORE_STARTUP_PARAMETERS=extra_float_digits" \
  -e "PGBOUNCER_AUTH_QUERY=SELECT usename, passwd FROM pg_shadow WHERE usename=\$1" \
  -e "PGBOUNCER_AUTH_USER=postgres" \
  -e "PGBOUNCER_QUERY_WAIT_TIMEOUT=0" \
  -e "PGBOUNCER_MAX_CLIENT_CONN=1000" \
  -e "PGBOUNCER_DEFAULT_POOL_SIZE=100" \
  -e "PGBOUNCER_MIN_POOL_SIZE=30" \
  -e "PGBOUNCER_RESERVE_POOL_SIZE=3" \
  -e "PGBOUNCER_SERVER_IDLE_TIMEOUT=1800" \
  -p 6432:6432 \
  -d bitnami/pgbouncer:latest